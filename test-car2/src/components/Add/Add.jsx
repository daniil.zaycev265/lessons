import React, { useState } from "react";
import "./Add.css";
import { Input, Button } from "antd";

const Add = (props) => {
  const { onCreate } = props;

  const [completedCheck, setCompletedCheck] = useState(false);
  const [textBrand, setTextBrand] = useState("");
  const [textModel, setTextModel] = useState("");
  const [textStatus, setTextStatus] = useState("");
  const [textYear, setTextYear] = useState("");
  const [textVin, setTextVin] = useState("");

  const submitHandler = () => {
    if (textBrand.trim()) {
      onCreate(
        completedCheck,
        textBrand,
        textModel,
        textStatus,
        textYear,
        textVin
      );
      setCompletedCheck(false);
      setTextBrand("");
      setTextModel("");
      setTextStatus("");
      setTextYear("");
      setTextVin("");
    }
  };

  const onSaveChangeBrand = (event) => setTextBrand(event.target.value);
  const onSaveChangeModel = (event) => setTextModel(event.target.value);
  const onSaveChangeStatus = (event) => setTextStatus(event.target.value);
  const onSaveChangeYear = (event) => setTextYear(event.target.value);
  const onSaveChangeVin = (event) => setTextVin(event.target.value);

  return (
    <form className="aside">
      <Input
        className="addForm-text"
        placeholder="Введите марку..."
        value={textBrand}
        onChange={onSaveChangeBrand}
      />
      <Input
        className="addForm-text"
        placeholder="Введите модель..."
        value={textModel}
        onChange={onSaveChangeModel}
      />
      <Input
        className="addForm-text"
        placeholder="Введите статус..."
        value={textStatus}
        onChange={onSaveChangeStatus}
      />
      <Input
        className="addForm-text"
        placeholder="Введите год производства..."
        value={textYear}
        onChange={onSaveChangeYear}
      />
      <Input
        className="addForm-text"
        placeholder="Введите VIN-номер..."
        value={textVin}
        onChange={onSaveChangeVin}
        minLength="17"
        maxLength="17"
      />
      <Button className="button" type="primary" onClick={submitHandler}>
        Добавить
      </Button>
    </form>
  );
};

export default Add;
