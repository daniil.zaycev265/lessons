import React from "react";

const SearchCar = (props) => {
  return (
    <input
      className="input contactCard__search"
      type="text"
      placeholder="Search for a contact..."
      value={props.query}
      onChange={props.onChangeQuery}
    />
  );
};

export default SearchCar;
