export const getRequest = (url) =>
  new Promise((resolve, reject) => {
    let request = new XMLHttpRequest();
    request.open("GET", url, true);
    request.onload = () => {
      if (request.status < 400) {
        resolve(JSON.parse(request.response));
      } else {
        reject("Код ошибки: " + request.status);
      }
    };
    request.addEventListener("error", () => {
      reject("network error");
    });
    request.send();
  });

export const postRequest = (url, data) =>
  new Promise((resolve, reject) => {
    const json = JSON.stringify(data);
    const request = new XMLHttpRequest();

    request.open("POST", url, true);
    request.setRequestHeader("Content-type", "application/json; charset=utf-8"); // Задаю в запросе заголовок
    request.onload = () => {
      if (request.status < 400) {
        resolve(JSON.parse(request.response));
      } else {
        reject("Код ошибки: " + request.status);
      }
    };
    request.addEventListener("error", () => {
      reject("network error");
    });
    request.send(json);
  });

export const deleteRequest = (url, id) =>
  new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();
    const userUrl = url + "/" + id;

    request.open("DELETE", userUrl, true);
    request.onload = () => {
      if (request.status < 400) {
        resolve(JSON.parse(request.response));
      } else {
        reject("Код ошибки: " + request.status);
      }
    };
    request.addEventListener("error", () => {
      reject("network error");
    });
    request.send(null);
  });

export const updateRequest = (url, data, id) =>
  new Promise((resolve, reject) => {
    const json = JSON.stringify(data);
    const request = new XMLHttpRequest();
    const userUrl = url + "/" + id;

    request.open("PUT", userUrl, true);
    request.setRequestHeader("Content-type", "application/json; charset=utf-8");
    request.onload = () => {
      if (request.status < 400) {
        resolve(JSON.parse(request.response));
      } else {
        reject("Код ошибки: " + request.status);
      }
    };
    request.addEventListener("error", () => {
      reject("network error");
    });
    request.send(json);
  });

// Способы для обработки события
// Какими способами можно повесить на кнопку функцию по клику?

// Есть три способа назначения обработчиков событий:
// Атрибут HTML: onclick="...".
// DOM-свойство: elem.onclick = function.
// Специальные методы: elem.addEventListener(event, handler[, phase]) для добавления, removeEventListener для удаления.
