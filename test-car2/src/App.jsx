import { useState, useEffect } from "react";
import "./App.css";
import List from "./components/List";
import Add from "./components/Add";
import Header from "./components/Header";
import { Tag } from "antd";
import { getRequest, deleteRequest, updateRequest } from "./api/request";

function App() {
  const [cars, setCars] = useState([]);
  useEffect(() => {
    getRequest("https://64e780cfb0fd9648b7900c98.mockapi.io/cars")
      .then((data) => {
        setCars(data);
      })
      .catch(() => {
        console.log("error");
      });
  }, []);

  const onSaveCars = (id, newBrand, newModel, newStatus, newYear, newVin) => {
    let changedCar = cars.find((item) => item.id === id);
    changedCar.brand = newBrand;
    changedCar.model = newModel;
    changedCar.status = newStatus;
    changedCar.year = newYear;
    changedCar.vin = newVin;
    updateRequest(
      "https://64e780cfb0fd9648b7900c98.mockapi.io/cars",
      changedCar,
      id
    )
      .then((car) => {
        setCars(cars.map((item) => (item.id === id ? car : item)));
      })
      .catch((errorText) => console.log(errorText));
  };

  const onDeleteCars = (id) => {
    deleteRequest("https://64e780cfb0fd9648b7900c98.mockapi.io/cars", id)
      .then((car) => {
        setCars(cars.filter((item) => item.id !== car.id));
      })
      .catch(() => {
        console.log("error");
      });
  };

  const numberSelectedCars = cars.filter((car) => car.completed === true);

  const onToggleCar = (id) => {
    const checkedCars = cars.map((car) => {
      if (car.id === id) {
        car.completed = !car.completed;
      }
      return car;
    });
    setCars(checkedCars);
    localStorage.setItem("carsData", JSON.stringify(checkedCars));
  };

  // const onSave = (editedCar) => {
  //   const editedCars = cars.map((car) =>
  //     car.id === editedCar.id ? editedCar : car
  //   );
  //   setCars(editedCars);
  //   localStorage.setItem("carsData", JSON.stringify(editedCars));
  // };

  // const onRemove = (id) => {
  //   const filtredCars = cars.filter((todo) => todo.id !== id);
  //   setCars(filtredCars);
  //   localStorage.setItem("carsData", JSON.stringify(filtredCars));
  // };

  const onAdd = (completed, brand, model, status, year, vin) => {
    setCars([
      ...cars,
      {
        id: Date.now(),
        completed: completed,
        brand: brand,
        model: model,
        status: status,
        year: year,
        vin: vin,
      },
    ]);
    localStorage.setItem(
      "carsData",
      JSON.stringify([
        ...cars,
        {
          id: Date.now(),
          completed: completed,
          brand: brand,
          model: model,
          status: status,
          year: year,
          vin: vin,
        },
      ])
    );
  };

  const onFilterCheckCar = () => {
    // eslint-disable-next-line array-callback-return
    const filtredCheckCar = numberSelectedCars.map((checkCar) => {
      if (checkCar.completed === true) {
        return (
          <Tag className="tag-car">
            {checkCar.brand} {checkCar.model}, {checkCar.year}
          </Tag>
        );
      }
    });
    return filtredCheckCar;
  };

  return (
    <div className="app">
      <Header title="Реестр автомобилей" />
      <div className="second-window">
        <Add onCreate={onAdd} />
      </div>
      <div>{onFilterCheckCar()}</div>
      {cars.length ? (
        <List
          onToggle={onToggleCar}
          onSaveCars={onSaveCars}
          cars={cars}
          onDeleteCars={onDeleteCars}
        />
      ) : (
        <h3>Автомобилей нет!</h3>
      )}
      <div>
        {numberSelectedCars.length === 0 ? (
          ""
        ) : (
          <h2>{`Выбрано ${numberSelectedCars.length} автомобиля`}</h2>
        )}
      </div>
    </div>
  );
}

export default App;
