import "./Item.css";

function Item(props) {
  const { todo, onRemove, onChange, index } = props;
  const { id, title, completed } = todo;
  const classes = [];

  if (todo.completed) {
    classes.push("done");
  }

  const onHandleRemove = () => onRemove(id);

  return (
    <li>
      <span className={classes.join(" ")}>
        <input
          type="checkbox"
          onChange={() => onChange(id)}
          checked={completed}
        />
        <strong>{index + 1}</strong>
        &nbsp;
        {title}
      </span>
      <button onClick={onHandleRemove}>Удалить</button>
    </li>
  );
}

export default Item;
