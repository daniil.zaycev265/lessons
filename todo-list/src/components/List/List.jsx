import React from "react";
import "./List.css";
import Item from "../Item";

function List({ todos, onToggle, onRemove }) {
  return (
    <ul>
      {todos.map((todo, index) => {
        return (
          <Item
            todo={todo}
            key={todo.id}
            index={index}
            onChange={onToggle}
            onRemove={onRemove}
          />
        );
      })}
    </ul>
  );
}

export default List;
