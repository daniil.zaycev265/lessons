import React, { useState } from "react";
import "./Add.css";

const Add = ({ onCreate }) => {
  const [text, setText] = useState("");

  const submitHandler = (event) => {
    event.preventDefault();

    if (text.trim()) {
      onCreate(text);
      setText("");
    }
  };

  return (
    <form onSubmit={submitHandler}>
      <input value={text} onChange={(event) => setText(event.target.value)} />
      <button type="submit">Добавить</button>
    </form>
  );
};

export default Add;
