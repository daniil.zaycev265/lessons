import { useState } from "react";
import "./App.css";
import List from "./components/List";
import Add from "./components/Add";

function App() {
  const [todos, setTodos] = useState([
    { id: 1, title: "Позавтракать", completed: false },
    { id: 2, title: "Помыться", completed: false },
    { id: 3, title: "Почистить зубы", completed: false },
    { id: 4, title: "Cесть за учебу", completed: false },
  ]);

  const onToggle = (id) => {
    setTodos(
      todos.map((todo) => {
        if (todo.id === id) {
          todo.completed = !todo.completed;
        }
        return todo;
      })
    );
  };

  const onRemove = (id) => {
    setTodos(todos.filter((todo) => todo.id !== id));
  };

  const onAdd = (title) => {
    setTodos(
      todos.concat([
        {
          title: title,
          id: Date.now(),
          completed: false,
        },
      ])
    );
  };

  return (
    <div className="app">
      <h1>Список задач</h1>
      {todos.length ? (
        <List todos={todos} onToggle={onToggle} onRemove={onRemove} />
      ) : (
        <h3>No todos!</h3>
      )}
      <Add onCreate={onAdd} />
    </div>
  );
}

export default App;
