// №1 Prototype

// const person = new Object({
//   name: "Maxim",
//   age: 25,
//   greet: function () {
//     console.log("Greet!");
//   },
// });

// Object.prototype.sayHello = function () {
//   console.log("Hello");
// };

// const lena = Object.create(person);
// lena.name = "Elena";

// const str = 'I am string'

// ============================

// №2 context

// function hello() {
//   console.log("Hello", this);
// }

// const person = {
//   name: "Vladilen",
//   age: 25,
//   sayHello: hello,
//   sayHelloWindow: hello.bind(document),
//   logInfo: function () {
//     console.group();
//     console.log(`Name is ${this.name}`);
//     console.log(`Age is ${this.age}`);
//   },
// };

// const lena = {
//   name: "Elena",
//   age: 23,
// };

// person.logInfo.bind(lena)();

// ????????????????????????

// №3 Замыкания

// function createCalcFunction(n) {
//   return function () {
//     console.log(1000 * n);
//   };
// }

// const calc = createCalcFunction(42);

// calc();

// =====

// function createIncrementor(n) {
//   return function (num) {
//     return n + num;
//   };
// }

// const addOne = createIncrementor(1);
// const addTen = createIncrementor(19);

// console.log(addOne(10));
// console.log(addOne(41));

// console.log(addTen(10));
// console.log(addTen(41));

// function urlGenerator(domain) {
//   return function (url) {
//     return `https://${url}.${domain}`;
//   };
// }

// const comUrl = urlGenerator("com");
// console.log(comUrl("youtube"));

// function fullName(lastName) {
//   return function (firstName) {
//     return `My name is ${firstName} ${lastName}`;
//   };
// }

// const infoPeople = fullName("Zaycev");
// console.log(infoPeople("Daniil"));

// № 4 Асинхронность

// console.log("Start");
// console.log("Start 2");

// function timeout2sec() {
//   console.log("timeout2sec");
// }

// setTimeout(function () {
//   console.log("Inside timeout, after 2000 seconds");
// }, 5000);

// setTimeout(timeout2sec, 2000);

// console.log("End");

// ES6

// const animal = {
//   name: 'Animal',
//   age:5,
//   hasTail: true
// }

// const people = [
//   { name: "Владилен", age: 25, budget: 40000 },
//   { name: "Елена", age: 17, budget: 3400 },
//   { name: "Игорь", age: 49, budget: 50000 },
//   { name: "Михаил", age: 15, budget: 1800 },
//   { name: "Василиса", age: 24, budget: 25000 },
//   { name: "Виктория", age: 38, budget: 2300 },
// ];

// for (let i = 0; i < people.length; i++) {
//   console.log(people[i]);
// }
// ===========================================
// for (let person of people) {
//   console.log(person);
// }
// ===========================================

// ForEach - перебор массива
// people.forEach(function (person, index, pArr) {
//   console.log(person);
//   console.log(index);
//   console.log(pArr);
// });
// ===========================================
// people.forEach(person => console.log(person);
// ===========================================

// Map - Он вызывает функцию для каждого элемента массива и возвращает массив результатов выполнения этой функции.
// const newPeople = people.map((person) => `${person.name} (${person.age})`);
// console.log(newPerson);
// ===========================================

// Filter - возвращает массив из всех подходящих элементов
// const adults = [];
// for (let i = 0; i < people.length; i++) {
//   if (people[i].age >= 18) {
//     adults.push(people[i]);
//   }
// }

// console.log(adults);
// ===========================================
// const adults = people.filter((person) => {
//   if (person.age >= 18) {
//     return true;
//   }
// });
// console.log(adults);
// ===========================================
// const adults = people.filter((person) => person.age >= 18);
// console.log(adults);
// ===========================================

// Reduce - вычисления единого значения на основе всего массива.

// let amount = 0;
// for (let i = 0; i > people.length; i++) {
//   amount += people[i].budget;
// }

// console.log(amount);
// ===========================================
// const amount = people.reduce((total, person) => {
//   return total + person.budget;
// }, 0);
// console.log(amount);
// ===========================================
// const amount = people.reduce((total, person) => total + person.budget);
// console.log(amount);
// ===========================================

// Find -

// const igor = people.find((person) => person.name === "Игорь");
// console.log(igor); // получили объект по условию вышел

// FindIndex -

// const igorIndex = people.findIndex((person) => person.name === "Игорь");
// console.log(igorIndex); // получили индекс объекта по условию вышел

// ===========================================

// const amount = people
//   .filter((person) => people.budget > 3000)
//   .map((person) => {
//     return {
//       info: `${person.name} (${person.age})`,
//       budget: person.budget,
//     };
//   })
//   .reduce((total, person) => total + person.budget, 0);

// console.log(amount);

// +=========================================== Map,Set,WeakMap,WeakSet
