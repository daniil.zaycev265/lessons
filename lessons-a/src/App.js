import { useState } from "react";
import "./App.css";
import Number from "./Number";

function App() {
  const [numbers, setNumbers] = useState([0, 1, 2, 3, 4, 5]);
  const [text, setText] = useState("");

  const onHandleChangeTextField = (event) => {
    const newTextValue = event.target.value;
    setText(newTextValue);
  };

  const onAddToStart = () => {
    const n = Math.floor(Math.random() * 100);
    setNumbers([n, ...numbers]);

    // console.log([...a, ...b]);
    // console.log([...a, "Клубника"]);
    // console.log([a, ..."Клубника"]);
  };

  const onAddToEnd = () => {
    const n = Math.floor(Math.random() * 100);
    setNumbers([...numbers, n]);
    // console.log([...a, ...b]);
    // console.log([...a, "Клубника"]);
    // console.log([a, ..."Клубника"]);
  };

  const onAddValue = () => {
    setNumbers([...numbers, text]);
    setText("");
  };

  return (
    <div className="app">
      {numbers.map((e) => (
        <Number value={e} />
      ))}
      <button onClick={onAddToStart}>В начало</button>
      <button onClick={onAddToEnd}>В конец</button>
      <input value={text} onChange={onHandleChangeTextField} />
      <h1>{text}</h1>
      <button onClick={onAddValue}>Добавить из input</button>
    </div>
  );
}

export default App;
