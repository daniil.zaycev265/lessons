import "./App.css";
import Cars from "./Components/Cars/Cars";
import Header from "./Components/Header/Header";
import Add from "./Components/Add/Add";
import { useState } from "react";

function App() {
  const [cars, setCars] = useState([
    {
      id: 1,
      brand: "Toyota",
      model: "Camry",
      status: "Продано",
      year: 1999,
      vin: "21312fda",
    },
    {
      id: 2,
      brand: "BMW",
      model: "X4",
      status: "В наличии",
      year: 2018,
      vin: "fd2312a",
    },
    {
      id: 3,
      brand: "BMW",
      model: "X3",
      status: "Продано",
      year: 2020,
      vin: "fd5431a",
    },
  ]);

  const addCar = (car) => {
    setCars(cars.concat({ car }));
  };

  return (
    <div className="app">
      <Header title="Реестр автомобилей" />
      <main>
        <Cars cars={cars} />
      </main>
      <aside>
        <Add onAdd={addCar} />
      </aside>
    </div>
  );
}

export default App;
