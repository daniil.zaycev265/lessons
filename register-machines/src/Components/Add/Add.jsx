import React, { useState } from "react";
import "./Add.css";

const Add = ({ onAdd }) => {
  const [brand, setBrand] = useState("");
  const [model, setModel] = useState("");
  const [year, setYear] = useState("");
  const [status, setStatus] = useState("");
  const [vin, setVin] = useState("");
  return (
    <form>
      <input
        value={brand}
        type="text"
        onChange={(e) => setBrand(e.target.value)}
      />
      <input
        value={model}
        type="text"
        onChange={(e) => setModel(e.target.value)}
      />
      <input
        value={status}
        type="text"
        onChange={(e) => setStatus(e.target.value)}
      />
      <input
        value={year}
        type="text"
        onChange={(e) => setYear(e.target.value)}
      />
      <input value={vin} type="text" onChange={(e) => setVin(e.target.value)} />
      <button
        type="button"
        onClick={() =>
          onAdd({
            brand: brand,
            model: model,
            year: year,
            status: status,
            vin: vin,
          })
        }
      >
        Добавить
      </button>
    </form>
  );
};

export default Add;
