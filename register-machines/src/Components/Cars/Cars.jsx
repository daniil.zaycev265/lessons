import "./Cars.css";
import Car from "../Car/Car";

const Cars = ({ cars }) => {
  return (
    <div>
      {cars.length ? (
        cars.map((e) => <Car car={e} key={e.id} />)
      ) : (
        <h3>Автомобилей нет</h3>
      )}
    </div>
  );
};

export default Cars;
