import "./Car.css";
import { IoCloseCircleSharp, IoHammerSharp } from "react-icons/io5";

const Car = ({ car }) => {
  return (
    <div className="car">
      <IoCloseCircleSharp className="delete-icon" />
      <IoHammerSharp className="edit-icon" />
      <h3>
        {car.brand} {car.model}
      </h3>
      <p>{car.status}</p>
      <p>{car.year}</p>
      <p>{car.vin}</p>
    </div>
  );
};

export default Car;
