import React from "react";
import "./List.css";
import Item from "../Item";

function List(props) {
  const { cars, onRemove, onSave, onToggle } = props;

  return (
    <table className="table-layout">
      <thead className="ant-table-thead">
        <tr>
          <th className="table-header">Чекбокс:</th>
          <th className="table-header">Марка:</th>
          <th className="table-header">Модель:</th>
          <th className="table-header">Статус:</th>
          <th className="table-header">Год производства:</th>
          <th className="table-header">VIN - номер:</th>
        </tr>
      </thead>
      {cars.map((car) => {
        return (
          <Item
            onSave={onSave}
            car={car}
            key={car.id}
            onRemove={onRemove}
            onChange={onToggle}
          />
        );
      })}
    </table>
  );
}

export default List;
