import { useState } from "react";
import "./Item.css";
import { Button, Input, Checkbox } from "antd";

function Item(props) {
  const { car, onRemove, onSave, onChange } = props;

  const [data, setData] = useState(car);

  const onHandleChangeTextField = (event) =>
    setData({
      ...data,
      [event.target.name]: event.target.value,
    });

  const [mode, setMode] = useState("view");

  const onHandleRemove = () => onRemove(car.id);
  const onHandleEdit = () => {
    setMode("edit");
  };

  const onHandleSave = () => {
    setMode("view");
    onSave({
      id: car.id,
      completed: data.completed,
      brand: data.brand,
      model: data.model,
      status: data.status,
      year: data.year,
      vin: data.vin,
    });
  };

  return (
    <tbody>
      <tr>
        <td className="table-text">
          <Checkbox
            onChange={() => onChange(car.id)}
            value={data.completed}
            checked={car.completed}
          />
        </td>
        <td className="table-text">
          {mode === "view" ? (
            data.brand
          ) : (
            <Input
              className="editInput"
              type="text"
              value={data.brand}
              onChange={onHandleChangeTextField}
              name="brand"
            />
          )}
        </td>
        <td className="table-text">
          {mode === "view" ? (
            data.model
          ) : (
            <Input
              className="editInput"
              type="text"
              value={data.model}
              onChange={onHandleChangeTextField}
              name="model"
            />
          )}
        </td>
        <td className="table-text">
          {mode === "view" ? (
            data.status
          ) : (
            <Input
              className="editInput"
              type="text"
              value={data.status}
              onChange={onHandleChangeTextField}
              name="status"
            />
          )}
        </td>
        <td className="table-text">
          {mode === "view" ? (
            data.year
          ) : (
            <Input
              className="editInput"
              type="text"
              value={data.year}
              onChange={onHandleChangeTextField}
              name="year"
            />
          )}
        </td>
        <td className="table-text">
          {mode === "view" ? (
            data.vin
          ) : (
            <Input
              className="editInput"
              type="text"
              value={data.vin}
              onChange={onHandleChangeTextField}
              name="vin"
              minLength="17"
              maxLength="17"
            />
          )}
        </td>
        <td>
          <Button
            onClick={mode === "view" ? onHandleEdit : onHandleSave}
            type="primary"
          >
            {mode === "view" ? "Редактировать" : "Сохранить"}
          </Button>
          <Button onClick={onHandleRemove} type="primary">
            Удалить
          </Button>
        </td>
      </tr>
    </tbody>
  );
}

export default Item;
