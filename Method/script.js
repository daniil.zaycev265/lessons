// array methods

// const people = [
//     { name: 'Даниил', age: 17, budget: 40000 },
//     { name: 'Максим', age: 25, budget: 3400 },
//     { name: 'Егор', age: 49, budget: 50000 },
//     { name: 'Андрей', age: 15, budget: 1800 },
//     { name: 'Елена', age: 24, budget: 25000 },
//     { name: 'Никита', age: 38, budget: 2300 },
// ]

// at // принимает целое число и возвращает элемент с данным индексом
// const array1 = [5, 12, 8, 130, 44];
// let index = 2;
// console.log(`Using an index of ${index} the item returned is ${array1.at(index)}`);
// index = -2;
// console.log(`Using an index of ${index} item returned is ${array1.at(index)}`);
// console.log(array1.at(0))

// push добавляет один или более элементов в конец и возвращает новую длину массива.
// const array1 = [5, 12, 8, 130, 44];
// array1.push(2,5,6)
// console.log(array1);

// pop удаляет последний элемент из массива и возвращает его значение.
// const array1 = [5, 12, 8, 130, 44];
// var number = array1.pop();
// console.log(array1);
// console.log(number);

// fill заполняет все элементы массива от начального до конечного индексов одним значением.
// const array1 = [5, 12, 8, 130, 44];
// console.log(array1.fill(0, 2, 4));

// join объединяет все элементы массива в строку с заданным разделителем.
// const array1 = [5, 12, 8, 130, 44];
// console.log(array1.join());
// console.log(array1.join(''));
// console.log(array1.join('-'));

// shift удаляет первый элемент из массива и возвращает его значение. Этот метод изменяет длину массива.
// const myFish = ['ангел', 'клоун', 'мандарин', 'хирург'];
// console.log('myFish до: ' + myFish);
// const shifted = myFish.shift();
// console.log('myFish после: ' + myFish);
// console.log('Удалён этот элемент: ' + shifted);

// reverse на месте обращает порядок следования элементов массива. Первый элемент массива становится последним, а последний — первым.
// const array1 = ['one', 'two', 'three'];
// console.log('array1:', array1);
// const reversed = array1.reverse();
// console.log('reversed:', reversed);

// unshift добавляет один или более элементов в начало массива и возвращает новую длину массива.
// const arr = [1, 2];
// arr.unshift(0);
// arr.unshift(-2, -1); // = 5
// console.log(arr);

// includes определяет, содержит ли массив определённый элемент, возвращая в зависимости от этого true или false.
// const array1 = [1, 2, 3];
// console.log(array1.includes(2));
// console.log(array1.includes('at'));

// map создаёт новый массив с результатом вызова указанной функции для каждого элемента массива.
// const numbers = [1, 4, 9];

// const roots = numbers.map(Math.sqrt);
// console.log(roots);

// const doubles = numbers.map((num) => num * 2);
// console.log(doubles);

// =========

// const people = [
//     { name: 'Даниил', age: 17, budget: 40000 },
//     { name: 'Максим', age: 25, budget: 3400 },
//     { name: 'Егор', age: 49, budget: 50000 },
//     { name: 'Андрей', age: 15, budget: 1800 },
//     { name: 'Елена', age: 24, budget: 25000 },
//     { name: 'Никита', age: 38, budget: 2300 },
// ]

// const newPeople = people.map(person => {
//     return `${person.name} (${person.age})`
// })

// console.log(newPeople);

// filter создаёт новый массив со всеми элементами, прошедшими проверку, задаваемую в передаваемой функции.
// const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];
// const result = words.filter(word => word.length > 6);

// console.log(result);

// =======

// function isBigEnough(value) {
//     return value >= 10;
//   }

//   let filtered = [12, 5, 8, 130, 44].filter(isBigEnough);

// find возвращает значение первого найденного в массиве элемента, которое удовлетворяет условию переданному в callback функции. В противном случае возвращается undefined.
// const people = [
//     { name: 'Даниил', age: 17, budget: 40000 },
//     { name: 'Максим', age: 25, budget: 3400 },
//     { name: 'Егор', age: 49, budget: 50000 },
//     { name: 'Андрей', age: 15, budget: 1800 },
//     { name: 'Елена', age: 24, budget: 25000 },
//     { name: 'Никита', age: 38, budget: 2300 },
// ]

// const daniil = people.find(person => person.name === 'Даниил')
// console.log(daniil);

// every проверяет, удовлетворяют ли все элементы массива условию, заданному в передаваемой функции.

// function isBigEnough(element, index, array) {
//     return element >= 10;
// }
// [12, 5, 8, 130, 44].every(isBigEnough);   // false
// [12, 54, 18, 130, 44].every(isBigEnough); // true

// findIndex возвращает индекс в массиве, если элемент удовлетворяет условию проверяющей функции. В противном случае возвращается -1.
// const people = [
//     { name: 'Даниил', age: 17, budget: 40000 },
//     { name: 'Максим', age: 25, budget: 3400 },
//     { name: 'Егор', age: 49, budget: 50000 },
//     { name: 'Андрей', age: 15, budget: 1800 },
//     { name: 'Елена', age: 24, budget: 25000 },
//     { name: 'Никита', age: 38, budget: 2300 },
// ]

// const daniilIndex = people.findIndex(person => person.name === 'Даниил')
// console.log(daniilIndex);

// reduce
// let amount = 0
// for (let i = 0; i < people.length; i++) {
//     amount += people[i].budget
// }
// console.log(amount);

// for (let i = 0; i < people.length; i++) {
//     console.log(people[i]);
// }

// for (let person of people) {
//     console.log(person);
// }

// ForEach
// people.forEach(function (person, index, pArr) {
//     console.log(person);
//     console.log(index);
//     console.log(pArr);
// })

// people.forEach(person => console.log(person))

// Map
// const newPeople = people.map(person => {
//     return `${person.name} (${person.age})`
// })

// console.log(newPeople);

// Filter
// const adults = []
// for (let i = 0; i < people.length; i++) {
// if (people[i].age >= 18) {
//     adults.push(people[i])
// }
// }
// console.log(adults);

// const adults = people.filter(person => {
//     if (person.age >= 18) {
//         return true
//     }
// })
// console.log(adults);

// const adults = people.filter(person => person.age >= 18)
// console.log(adults);

// Reduce
// let amount = 0
// for (let i = 0; i < people.length; i++) {
//     amount += people[i].budget
// }
// console.log(amount);

// const amount = people.reduce((total, person) => {
//     return total + person.budget
// }, 0)
// console.log(amount);

// const amount = people.reduce((total, person) => total + person.budget, 0)
// console.log(amount);

// Find
//const daniil = people.find(person => person.name === 'Даниил')
// console.log(daniil);

// FindIndex

// const daniilIndex = people.findIndex(person => person.name === 'Даниил')
// console.log(daniilIndex);

// ===============
// const amount = people
//     .filter(person => person.budget > 3000)
//     .map (person => {
//         return {
//             info: `${person.name} (${person.age})`,
//             budget: Math.sqrt(person.budget)
//         }
//     })
//     .reduce((total, person) => total + person.budget, 0)

//     console.log(amount);

// string methods

// charAt возвращает указанный символ из строки.
// var anyString = 'Дивный новый мир';

// console.log("Символ по индексу 0   равен '" + anyString.charAt(0)   + "'");
// console.log("Символ по индексу 1   равен '" + anyString.charAt(1)   + "'");
// console.log("Символ по индексу 2   равен '" + anyString.charAt(2)   + "'");
// console.log("Символ по индексу 3   равен '" + anyString.charAt(3)   + "'");
// console.log("Символ по индексу 4   равен '" + anyString.charAt(4)   + "'");
// console.log("Символ по индексу 5   равен '" + anyString.charAt(5)   + "'");
// console.log("Символ по индексу 999 равен '" + anyString.charAt(999) + "'");

// concat объединяет текст из двух или более строк и возвращает новую строку.
// var hello = 'Привет, ';
// console.log(hello.concat('Кевин', ', удачного дня.'));

// startsWith помогает определить, начинается ли строка с символов указанных в скобках, возвращая, соответственно, true или false.
// var str = 'Быть или не быть, вот в чём вопрос.';

// console.log(str.startsWith('Быть'));        // true
// console.log(str.startsWith('не быть'));     // false
// console.log(str.startsWith('не быть', 9));  // true

// endsWith позволяет определить, заканчивается ли строка символами указанными в скобках, возвращая, соответственно, true или false.
// var str = 'Быть или не быть, вот в чём вопрос.';

// console.log(str.endsWith('вопрос.'));   // true
// console.log(str.endsWith('быть'));      // false
// console.log(str.endsWith('быть', 16));  // true

// includes проверяет, содержит ли строка заданную подстроку, и возвращает, соответственно true или false.
// var str = 'Быть или не быть вот в чём вопрос.';

// console.log(str.includes('Быть'));       // true
// console.log(str.includes('вопрос'));    // true
// console.log(str.includes('несуществующий')); // false
// console.log(str.includes('Быть', 1));    // false
// console.log(str.includes('БЫТЬ'));       // false

// indexOf возвращает индекс первого вхождения указанного значения в строковый объект String, на котором он был вызван, начиная с индекса fromIndex. Возвращает -1, если значение не найдено.
// 'Синий кит'.indexOf('Синий');   // вернёт  0
// 'Синий кит'.indexOf('Голубой');  // вернёт -1
// 'Синий кит'.indexOf('кит', 0);    // вернёт  6
// 'Синий кит'.indexOf('кит', 5);    // вернёт  6
// 'Синий кит'.indexOf('', 8);       // вернёт  8
// 'Синий кит'.indexOf('', 9);      // вернёт 9
// 'Синий кит'.indexOf('', 10);      // вернёт 9

// ?????????????????????

// lastIndexOf возвращает индекс последнего вхождения указанного значения в строковый объект String, на котором он был вызван, или -1, если ничего не было найдено. Поиск по строке ведётся от конца к началу, начиная с индекса fromIndex.

// 'канал'.lastIndexOf('а');     // вернёт 3
// 'канал'.lastIndexOf('а', 2);  // вернёт 1
// 'канал'.lastIndexOf('а', 0);  // вернёт -1
// 'канал'.lastIndexOf('ч');     // вернёт -1

// match возвращает получившиеся совпадения при сопоставлении строки с регулярным выражением. ????????????????

// padStart  заполняет текущую строку другой строкой (несколько раз, если нужно) так, что итоговая строка достигает заданной длины. Заполнение осуществляется в начале (слева) текущей строки.

// 'abc'.padStart(10);         // "       abc"
// 'abc'.padStart(10, "foo");  // "foofoofabc"
// 'abc'.padStart(6,"123465"); // "123abc"
// 'abc'.padStart(8, "0");     // "00000abc"
// 'abc'.padStart(1);          // "abc"

// padEnd дополняет текущую строку с помощью заданной строки (в конечном счёте повторяя), так чтобы результирующая строка достигла заданной длины. Дополнение применяется в конце (справа) текущей строки
// 'abc'.padEnd(10);         // "abc       "
// 'abc'.padEnd(10, "foo");  // "abcfoofoof"
// 'abc'.padEnd(6,"123456"); // "abc123"

// repeat конструирует и возвращает новую строку, содержащую указанное количество соединённых вместе копий строки, на которой он был вызван.
// 'абв'.repeat(-1);   // RangeError
// 'абв'.repeat(0);    // ''
// 'абв'.repeat(1);    // 'абв'
// 'абв'.repeat(2);    // 'абвабв'
// 'абв'.repeat(3.5);  // 'абвабвабв' (количество будет преобразовано в целое число)

// replace ??????????????????

// search

// slice извлекает часть строки и возвращает новую строку без изменения оригинальной строки.
// let str1 = 'Приближается утро.';
// let str2 = str1.slice(1, 8);
// let str3 = str1.slice(4, -2);
// let str4 = str1.slice(12);
// let str5 = str1.slice(30);

// console.log(str2); // ВЫВОД: риближа
// console.log(str3); // ВЫВОД: лижается утр
// console.log(str4); // ВЫВОД:  утро.
// console.log(str5); // ВЫВОД: ""

// split ???????????
// function splitString(stringToSplit, separator) {
//     var arrayOfStrings = stringToSplit.split(separator);

//     console.log('Оригинальная строка: "' + stringToSplit + '"');
//     console.log('Разделитель: "' + separator + '"');
//     console.log('Массив содержит ' + arrayOfStrings.length + ' элементов: ' + arrayOfStrings.join(' / '));
//   }

//   // Строчка из «Бури» Шекспира. Перевод Михаила Донского.
//   var tempestString = 'И как хорош тот новый мир, где есть такие люди!';
//   var monthString = 'Янв,Фев,Мар,Апр,Май,Июн,Июл,Авг,Сен,Окт,Ноя,Дек';

//   var space = ' ';
//   var comma = ',';

//   splitString(tempestString, space);
//   splitString(tempestString);
//   splitString(monthString, comma);

// substring возвращает подстроку строки между двумя индексами, или от одного индекса и до конца строки.
var anyString = "Mozilla";

// Отобразит 'Moz'
console.log(anyString.substring(0, 3));
console.log(anyString.substring(3, 0));

// Отобразит 'lla'
console.log(anyString.substring(4, 7));
console.log(anyString.substring(7, 4));

// Отобразит 'Mozill'
console.log(anyString.substring(0, 6));

// Отобразит 'Mozilla'
console.log(anyString.substring(0, 7));
console.log(anyString.substring(0, 10));

// toLowerCase возвращает значение строки, на которой он был вызван, преобразованное в нижний регистр.
console.log("АЛФАВИТ".toLowerCase()); // 'алфавит'

// toUpperCase  возвращает значение строки, на которой он был вызван, преобразованное в верхний регистр.
console.log("алфавит".toUpperCase()); // 'АЛФАВИТ'

// trim удаляет пробельные символы с начала и конца строки. Пробельными символами в этом контексте считаются все собственно пробельные символы (пробел, табуляция, неразрывный пробел и прочие) и все символы конца строки (LF, CR и прочие).
var orig = "   foo  ";
console.log(orig.trim()); // 'foo'

// trimStart удаляет пробельные символы с левого конца строки.

// trimEnd удаляет пробельные символы с правого конца строки.
