const names = ['Владилен', 'Елена', 'Игорь', 'Ксения']

// names.push('Алена') // добавить в конец массива
// names.unshift('Алена') // добавить в начало массива

// names.shift() // удалить первый элемент массива
// names.pop() // удалить последний элемент массива
// console.log('Names: ', names);

// console.log(names.reverse()); // переворачивает массив (меняет оригинал)
// console.log(names.toReversed()) // переворачивает массив (не меняет оригинал)
// const letters = ['a', 'c', 'd', 'b', 'e']
// console.log(letters.sort()); // сортирует массив изменяя его (меняет оригинал)
// console.log(letters.toSorted()); // сортирует массив изменяя его (не меняет оригинал)
// console.log(names.splice(2, 1)); // удаляет элемент массива, где "2" - элемент начала, а "1" сколько элементов хотим удалить (меняет оригинал)
// console.log(names.toSpliced(2, 1)); // удаляет элемент массива, где "2" - элемент начала, а "1" сколько элементов хотим удалить (не меняет оригинал)

// const greateWomen = 'Елена'
// names.indexOf(greateWomen) // находит заданный элемент массива и выводит его индекс
// console.log(index);
// names.With(index, 'Елена Великая') // Не меняет изначальный массив
// names[index] = 'Елена Великая'
// console.log(names[index]) // меняет изначальный массив
// console.log(names)

// const capitalNames = names.map(function(name) {
//     const newName = name + '!'
//     return newName
// }) // берет каждый элемент массива (Не меняет массив, а возвращает новый)
// console.log(capitalNames);

// const capitalNames = names.map(function(name) {
//     return name.toUpperCase()
// }) // берет каждый элемент массива и приводит к верхнему регистру (Не меняет массив, а возвращает новый)
// console.log(capitalNames);

// const capitalNames = names.map(function(name) {
//     return name.toLowerCase()
// }) // берет каждый элемент массива и приводит к нижнему регистру (Не меняет массив, а возвращает новый)
// console.log(capitalNames);

// const capitalNames = names.map(function(name, index) {
//     if (index === 1) {
//         return 'Елена Великая'
//     } 
//     return name
// }) 
// console.log(capitalNames);

// console.log(names.includes('Игорь')) // проверяет, есть ли элемент в массиве
// console.log(names.indexOf('Игорь') !== -1);

const people = [
    {name: 'Владилен', budget: 4200},
    {name: 'Елена', budget: 15100},
    {name: 'Игорь', budget: 300},
    {name: 'Ксения', budget: 7520} ,
]

// let findedPerson

// for (let person of people) {
//     if (person.budget === 7520) {
//         findedperson = person
//     }
// }

// const findedPerson = people.find(function(person) { // возвращает значение первого найденного элемента
//     return person.budget === 7520
//     // if (person.budget === 7520) {
//     //     return true
//     // }
// })

// const finded = people.findIndex(function(person) { // возвращает индекс первого найденного элемента
//     return person.budget === 7520
// })

// console.log(people.with(finded, 42))

// const finded = people.find((p) => p.budget === 7520) 

// console.log(finded); //

// let sumBudget = 0

// const filtered = people.filter(function(p) {
// return p.budget > 5000
// })

// filtered.forEach(function(p) {
//     sumBudget += p.budget
// }) 

// console.log(sumBudget);

// const byBudget = (p) => p.budget > 5000
// const pickBudget = (p) => p.budget

// const sumBudget = people
//     .filter(byBudget)
//     .map(pickBudget)
//     .reduce((acc, p) => acc + p, 0)

// console.log(sumBudget);

// const string = 'Привет, как дела?'
// const reversed = string
// .split('')
// .toReversed()
// .join('!')
// .split('')
// .filter((c) => c !== '!')
// .join('') // превращает строки в массив

// console.log(reversed);


